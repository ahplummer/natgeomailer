FROM python:3
# Install dependencies
RUN apt-get update && apt-get install -y \
    python3-pip

# git the git.
WORKDIR /data
RUN git clone https://gitlab.com/ahplummer/natgeomailer.git /data/natgeomailer
WORKDIR /data/natgeomailer
RUN pip3 install -r requirements.txt
CMD cd /data/natgeomailer && git pull && python3 NatGeo.py
