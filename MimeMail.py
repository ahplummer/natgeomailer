import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
import os

# http://docs.python.org/3.3/library/email-examples.html
class MimeMail:
    #static member var
    server = ""
    user = ""
    password = ""

    def __init__(self, server, user, password):
        MimeMail.server = server
        MimeMail.user = user
        MimeMail.password = password

    def sendInlineImageEmail(self, TO, FROM, subject, message, fileNames, subTypes, ids):
        msg = MIMEMultipart('related')
        msg['Subject'] = subject
        msg['From'] = FROM
        msg['To'] = TO
        msg.preamble = 'This is a multi-part message in MIME format.'

        # Encapsulate the plain and HTML versions of the message body in an
        # 'alternative' part, so message agents can decide which they want to display.
        msgAlternative = MIMEMultipart('alternative')
        msg.attach(msgAlternative)

        msgText = MIMEText('This is the alternative plain text message.')
        msgAlternative.attach(msgText)

        # We reference the image in the IMG SRC attribute by the ID we give it below
        msgText = MIMEText(message, 'html')
        msgAlternative.attach(msgText)

        # This example assumes the image is in the current directory
        i = 0
        if fileNames != None:
            for fileName in fileNames:
                if fileName != None and fileName != "" and os.path.exists(fileName):
                    print(fileName)
                    fp = open(fileName, 'rb')
                    msgImage = MIMEImage(fp.read(), _subtype=subTypes[i])
                    fp.close()
                    # Define the image's ID as referenced above
                    msgImage.add_header('Content-ID', '<' + ids[i] + '>')
                    msg.attach(msgImage)
                    i = i + 1
        # The actual mail send
        server = smtplib.SMTP(MimeMail.server)
        if MimeMail.user != "" and MimeMail.user != None:
            print('Starting TTLS with user %s' % MimeMail.user)
            server.starttls()
            server.login(MimeMail.user, MimeMail.password)
        server.sendmail(FROM, TO, msg.as_string())
        server.quit()
