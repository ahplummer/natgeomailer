import argparse
import urllib.request
import os
from bs4 import BeautifulSoup
import ctypes
from MimeMail import MimeMail
from email.mime.text import MIMEText
from shutil import copyfile
import filecmp
import PIL
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw
url= 'http://photography.nationalgeographic.com/photography/photo-of-the-day/'

class Caption():
    def __init__(self, value):
        self.Value = value

def DownloadPicOfTheDay(localdirectory, url, caption):
    localFileName = None
    response = urllib.request.urlopen(url)

    html = response.read()
    soup = BeautifulSoup(html, 'html.parser')
    primaryphoto = soup.find("meta", {"property":"og:image"})
    primarydescription = soup.find("meta", {"property":"og:description"})
    caption.Value = primarydescription['content'].encode('utf-8')
    if primaryphoto != None:
        picurl = primaryphoto['content']
        localFileName = localdirectory + os.path.sep + 'local.jpg'
        responsepic = urllib.request.urlretrieve(picurl, localFileName)
    return localFileName

def ApplyCaption(caption, fileName):
    #font = ImageFont.truetype("/usr/share/fonts/dejavu/DejaVuSans.ttf", 25)
    font = ImageFont.truetype("DejaVuSerif.ttf", 34)
    #img = Image.new("RGBA", (200,200), (120,20,20))
    img = Image.open(fileName)
    draw = ImageDraw.Draw(img)
    draw.text((0,0), bytes.decode(caption.Value).replace(' in this National Geographic Your Shot Photo of the Day', ''), (255,255,0), font=font)
    draw = ImageDraw.Draw(img)
    img.save(fileName)

def SetBackground(localFile):
    SPI_SETDESKWALLPAPER = 20
    SPIF_SENDCHANGE = 3
    #localFile = localFile.replace('jpg','bmp')
    if os.path.exists(localFile):
        print (localFile + ' exists.')
        ret = ctypes.windll.user32.SystemParametersInfoA(SPI_SETDESKWALLPAPER, 0, localFile, SPIF_SENDCHANGE)
        print (ret)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Grabs latest Photo of the day from NatGeo')
    parser.add_argument('-dir','--dir', help='Directory to store', required = False)

    args = vars(parser.parse_args())
    if args['dir'] != None:
        localdirectory = args['dir']
    else:
        localdirectory = '.'
    if localdirectory == '.':
        localdirectory = os.path.dirname(os.path.realpath(__file__))

    CurrentPicture = localdirectory + os.path.sep + 'DailyNatGeo.jpg'
    OldPicture = 'OldDailyNatGeo.jpg'
    recipient = os.environ['RECIPIENT']
    fromaddress = os.environ['SMTP_USER']
    server = os.environ['SMTP_SERVER']
    password=os.environ['SMTP_PASSWORD']
    em = MimeMail(server, fromaddress, password)
    caption = Caption('')
    downloadedFile = DownloadPicOfTheDay(localdirectory, url, caption)
#    ApplyCaption(caption, downloadedFile)
    if downloadedFile != None:
        #SetBackground(downloadedFile)
        if os.path.exists(CurrentPicture):
            copyfile(CurrentPicture, OldPicture)
        copyfile(downloadedFile, CurrentPicture)
        Pictures = []
        SubTypes = []
        Ids = []
        message = bytes.decode(caption.Value)
        subject = 'Daily National Geographic Photograph'
        if os.path.exists(OldPicture) and filecmp.cmp(OldPicture, CurrentPicture):
            message = '<br/><b>NatGeo is the same as before.</b>'
        else:
            message += str('<br/><br><img src="cid:natgeo" width="1024">')
            Pictures.append(CurrentPicture)
            SubTypes.append('png')
            Ids.append('natgeo')

        em.sendInlineImageEmail(recipient,fromaddress,subject,message,Pictures,SubTypes,Ids)


